<?php 
/*
Template Name: Community Page
*/
get_header(); ?>

<div class="lineup content page clearfix">
    
    
    <div class="latest-post">
		<h1>Community Notice Board</h1>
		<p>Peel back the facade of a summer camping event and you'll find Festival One is a true celebration of creativity, community and awesome bands.</p>
		<a title="Share" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>">Share this page</a>
	</div>
	
	
	<div class="news clearfix">    
    
	    <?php $args = array( 'post_type' => 'community', 'posts_per_page' => 999, 'order' => 'DESC' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			
			 <div class="third">
		            <a href="<?php the_permalink();?>">
						<?php if ( has_post_thumbnail() ) { 
							the_post_thumbnail('header'); 
						}?>
						<h3><?php the_title();?></h3>
					</a>
	            </div>      
			
			<?php endwhile; ?>
	</div>
 
</div>

<?php get_footer(); ?>
     