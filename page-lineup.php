<?php get_header(); ?>

<div class="lineup content clearfix">
    
    
    <div class="latest-post">
		<h1>Festival One 2016 Lineup</h1>
		<p>The 2016 Festival One family is looking sharp. Worship ensemble Bethel Music, wordsmith NF, folk duo All Sons and Daughters, Brady Toops, Ike Ndolo and a bevy of other carefully curated artists to keep your ears excited.</p>
		<!--<p>The musicians, speakers and events are piling up. Keep an ear to the ground, or better still, sign up to our mailing list below to keep up to date.</p><p>We can’t wait to share with you who we’ve got lined up!</p>-->
		<a title="Share" target="_blank" href="#musicians">Musicians</a> | <a title="Share" target="_blank" href="#speakers">Speakers</a>
	</div>
	
	
	<div class="news clearfix">    
    	
    	<div id="musicians" style="float:left;"> 	
	    	<h1>Musicians</h1>
	    	<?php $args = array( 'post_type' => 'artists', 'posts_per_page' => 999, 'category_name' => 'musicians', 'order' => 'DESC' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			
			 <div class="third">
		            <a href="<?php the_permalink();?>">
						<?php if ( has_post_thumbnail() ) { 
							the_post_thumbnail('header'); 
						}?>
						<h3><?php the_title();?></h3>
					</a>
	            </div>      
			
			<?php endwhile; ?>
    	</div>
			
		<div id="speakers" style="float:left;">
			<h2>Speakers</h2>
			<?php $args = array( 'post_type' => 'artists', 'posts_per_page' => 999, 'category_name' => 'speakers', 'order' => 'DESC' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			
			 <div class="third">
		            <a href="<?php the_permalink();?>">
						<?php if ( has_post_thumbnail() ) { 
							the_post_thumbnail('header'); 
						}?>
						<h3><?php the_title();?></h3>
					</a>
	            </div>      
			
			<?php endwhile; ?>
		</div>
	</div>
 
</div>

<?php get_footer(); ?>
     