<?php
/*
Template Name: Info Page
*/

get_header(); ?>

<div class="content clearfix page">
	
	<div class="latest-post">
		<h1><?php the_field('page_title'); ?></h1>
		<p><?php the_field('page_intro'); ?></p>
	</div>
	
	
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="image-header">
			<?php the_post_thumbnail('header'); ?>
		</div>
	<?php 	}?>
	
	<?php while ( have_posts() ) : the_post(); ?>

		<?php // check for rows (parent repeater)
		if( have_rows('section_repeater') ): ?>
			
			<?php 
			// loop through rows (parent repeater)
			while( have_rows('section_repeater') ): the_row(); ?>
				<section class="clearfix">
					<div class="intro">
						<h2><?php the_sub_field('section_title'); ?></h2> 
					</div>
					<div class="main">
					<?php 
					// check for rows (sub repeater)
					if( have_rows('section_content') ): ?>
						<?php 
	
						// loop through rows (sub repeater)
						while( have_rows('section_content') ): the_row();?>
						
							<div class="<?php the_sub_field('class'); ?>">
								<h3><?php the_sub_field('content_title'); ?></h3>
								<p><?php the_sub_field('content_copy'); ?></p>
							</div>
	
						<?php endwhile; ?>
					
						
					<?php endif; //if( get_sub_field('section_content') ): ?>
					</div>	
					</section>
			<?php endwhile; // while( has_sub_field('section_repeater') ): ?>
				
		<?php endif; // if( get_field('section_repeater') ): ?>
	
	<?php endwhile; // end of the loop. ?>

	
</div>


<?php get_footer(); ?>