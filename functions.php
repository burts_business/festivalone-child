<?php //Functions Page
add_action( 'init', 'create_posttype' );
function create_posttype() {
	register_post_type( 'artists',
		array(
			'labels' => array(
				'name' =>  'Artists',
				'singular_name' => 'Artist',
				'add_new' => 'Add new Artist',
	            'edit_item' => 'Edit Artist',
	            'new_item' => 'New Artist',
	            'view_item' => 'View Artist',
	            'search_items' => 'Search Artists',
	            'not_found' => 'No Artists found',
	            'not_found_in_trash' => 'No Artists found in Trash'
			),
			'public' => true,
			'menu_position' => 5,
			'has_archive' => true,
			'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt'
			),
			'rewrite' => array('slug' => 'artists', 'with_front' 	=> false ),
			'taxonomies' => array('category')
		)
	);
}
function add_custom_types_to_tax( $query ) {
if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
// Get all your post types
$post_types = get_post_types();
$query->set( 'post_type', $post_types );
return $query;
}
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );
add_theme_support( 'post-thumbnails' ); 
//The excerpt
function excerpt_read_more_link($output) {
 global $post;
 return $output . '<a class="button" href="'. get_permalink($post->ID) . '"> Read More</a>';
}
add_filter('the_excerpt', 'excerpt_read_more_link');
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'community',
    array(
      'labels' => array(
				'name' =>  'Community',
				'singular_name' => 'Community',
				'add_new' => 'Add new Community Item',
	            'edit_item' => 'Edit Community Item',
	            'new_item' => 'New Community Item',
	            'view_item' => 'View Community',
	            'search_items' => 'Search Community',
	            'not_found' => 'No Community found',
	            'not_found_in_trash' => 'No Community found in Trash'
			),
			'public' => true,
			'menu_position' => 5,
			'has_archive' => true,
			'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt'
			),
			'rewrite' => array('slug' => 'community', 'with_front' 	=> false ),
			'taxonomies' => array('category')
		)
	);
}
add_image_size( 'header', 1199, 468, array( 'center', 'center' ) ); // Hard crop center, center
?>