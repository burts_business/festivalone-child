<?php get_header(); ?>

<div id="ticket-page" class="content clearfix page">
	
	<div class="latest-post">
		<h1>Festival One 2016 Tickets</h1>
		<p>Tickets for sale include adults, children and caravan sites. A word from the wise, the longer you leave it the more you're going to have to pay for your ticket.</p>
		<a title="Share" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>">Share this page</a>
	</div>
	
	<section class="clearfix">
		<!--<div class="five-col">
			<h2>Tier Two</h2> 
		</div>-->
		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016"><div class="price">
				<h3>Child</h3>
				<p>$80</p>
			</div></a>
			<p>5 - 12 Years Old</p>
		</div>
		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016"><div class="price">
				<h3>Student</h3>
				<p>$139</p>
			</div></a>
			<p>With Valid Student I.D.</p>
		</div>
		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016"><div class="price">
				<h3>Adult</h3>
				<p>$159</p>
			</div></a>
		
		</div>
		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016"><div class="price">
				<h3>Family</h3>
				<p>Price Varies</p>
			</div></a>
			<p>Click for details</p>
		</div>

		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016"><div class="price">
				<h3>Caravans</h3>
				<p>On Sale Now</p>
			</div></a>
			<p>Click for details</p>
		</div>
	</section>
	
	<section class="clearfix">
		<div class="five-col">
			<h2>Half Festival</h2> 
		</div>
		<div class="five-col">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016">
				<div class="price">
					<h3>Child</h3>
					<p>$45</p>
				</div>
			</a>
			<p>5 - 12 Years Old</p>
		</div>
		<div class="five-col">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016">
				<div class="price">
					<h3>Adult</h3>
					<p>$85</p>
				</div>
			</a>

		</div>
	</section>
	
	
	<section class="col-container not-sale clearfix">
		<div class="five-col col-left blue">
			<h2 class="blue">Second Release</h2> 
			<p class="blue">SOLD OUT</p>
		</div>
		<div class="five-col">
			<div class="price">
				<h3>Child</h3>
				<p>...</p>
			</div>
			<p>5 - 12 Years Old</p>
		</div>
		<div class="five-col">
			<div class="price">
				<h3>Student</h3>
				<p>...</p>
			</div>
			<p>With Valid Student I.D.</p>
		</div>
		<div class="five-col">
			<div class="price">
				<h3>Adult</h3>
				<p>...</p>
			</div>
		
		</div>
		<div class="five-col">
			<div class="price">
				<h3>Caravans	</h3>
				<p>...</p>
			</div>
			<p>Click for details</p>
		</div>
	</section>


	
	<section class="clearfix not-sale">
		<div class="five-col">
			<h2>Earlybird</h2> 
			<p class="blue">Grab the cheapest Festival One ticket. Limited release.</p>
			<p class="blue">SOLD OUT</p>
		</div>
		
		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016"><div class="price">
				<h3>Child</h3>
				<p>SOLD OUT</p>
			</div></a>
			<p>Under 12 Years Old</p>
		</div>
		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016"><div class="price">
				<h3>Student</h3>
				<p>SOLD OUT</p>
			</div></a>
			<p>With Valid Student I.D.</p>
		</div>
		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016"><div class="price">
				<h3>Adult</h3>
				<p>SOLD OUT</p>
			</div></a>
			<i></i>
		</div>
		<div class="five-col tix">
			<a href="https://www.iticket.co.nz/events/2015/apr/festival-one-2016-campervan-and-caravan-sites" target="_blank"><div class="price">
				<h3>Caravans</h3>
				<p>On sale now</p>
			</div>
			<p>Click for details</p></a>
		</div>
	</section>
		
	

</div>


<?php get_footer(); ?>