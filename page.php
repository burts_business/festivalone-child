<?php get_header(); ?>

<div class="content clearfix page boring-page">
	
	<?php if( get_field('page_title') ): ?>
		<div class="latest-post">
			<h1><?php the_field('page_title'); ?></h1>
			<p><?php the_field('page_intro'); ?></p>
		</div>
	<?php endif; ?>
	
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="image-header">
			<?php the_post_thumbnail('header'); ?>
		</div>
	<?php 	}?>
	
	<?php while ( have_posts() ) : the_post(); ?>

				<section class="clearfix">
					<div class="intro">
						<h2><?php the_title(); ?></h2> 
					</div>
					<div class="main">
						<?php the_content(); ?>
					</div>	
					</section>
	
	
	<?php endwhile; // end of the loop. ?>

	
</div>

<?php get_footer(); ?>
