/*var userFeed = new Instafeed({
   get: 'user',
    userId: 1460154467,
    accessToken: '1460154467.467ede5.90d036f771a44d3b9bbebe17b8175e9d',
    resolution: 'standard_resolution',
    limit: 5
});
userFeed.run();*/

var $mobileToggle = $('header.mobile'),
	$mobileNav = $('header nav');


$mobileToggle.click(function(){
	$mobileNav.slideToggle();
});

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

	

var fixed = false;

$(document).scroll(function() {
    if( $(this).scrollTop() > 300 ) {
        if( !fixed ) {
            fixed = true;
            $('.secondary').css({ marginTop: '0px'});

        }
    } else {
        if( fixed ) {
            fixed = false;
			$('.secondary').css({ marginTop: '-150px'});
        }
    }
    
   
});

$("#close").click(function(){
    $(".mobile-nav").hide();
});

$(".trigger").click(function(){
    $(".mobile-nav").show();
});