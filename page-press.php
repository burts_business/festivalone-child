<?php get_header(); ?>

<div class="content">
	<div class="four-col col-left">
		<h2 class="blue">Press Package</h2>
		<p>We want you to own this. So we aren't doing any advertising. That we are leaving to you. We provide the goods and you do the rest. Print a poster. Take a gram. Do your thing.</p>
	</div>
	<div class="clearfix"></div>
	<div class="col-container">
		<div class="four-col col-left blue">
			<h2 class="blue">Logo</h2>
			
		</div>
		<div class="two-col">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/festival-one-logo.png" alt="Festival One Logo"/>
		</div>
		<div class="four-col printed">
			<a href="<?php bloginfo('stylesheet_directory'); ?>/images/festival-one-logo.pdf" class="button">high-res download</a> 
		</div>
	</div>
	<div class="col-container">
		<div class="four-col col-left blue">
			<h2 class="blue">Words</h2>
		</div>
		<div class="four-col">
			<h3>Press Release</h3>
			<p>The official Press Release for Festival One 2015</p>
			<a href="<?php bloginfo('stylesheet_directory'); ?>/assets/Festival-One-2015-Press-Release.pdf" class="button">Download</a>
		</div>
		<div class="four-col printed">
			<h3>The Open Letter</h3>
			<p>Read the Executive Director's Open Letter</p>
			<a href="/open-letter/" class="button">Read</a>
		</div>
		<div class="four-col">
			<!--<h3>Facebook Post</h3>
			<p>Switchfoot. Halfnoise.</p>
			<a href="#" class="button">Share</a>-->
		</div>
	</div>
	<!--<div class="col-container">
		<div class="four-col col-left blue">
			<h2 class="blue">Images</h2> 
		</div>
		<div class="four-col">
			<h3>Headliners</h3>
			<p>Festival One Headliners. Switchfoot. Halfnoise. Gungor. Paper Route.</p>
			<a href="#" class="button">Share</a>	
			
		</div>
		<div class="four-col">
			<h3>Switchfoot</h3>
			<p>Switchfoot return to New Zealand to play at the brand new Festival One.</p>
			<a href="#" class="button">Share</a>
		</div>
		<div class="four-col">
			<h3>Halfnoise</h3>
			<p>Highly acclaimed drummer headlining new festival with new music. Festival One.</p>
			<a href="#" class="button">Share</a>
		</div>
	</div>-->
	<div class="col-container">
		<div class="four-col col-left blue">
			<h2 class="blue">Other</h2> 
		</div>
		<div class="four-col">
			
			<p>Any other assets/information can be requested from info@festival-one.co.nz</p>
			<a href="mailto:info@festival-one.co.nz" class="blue">info@festival-one.co.nz</a>	
		</div>
		<!--<div class="four-col">
			<h3>Want to be a Photographer?</h3>
			<p>Contact our head of media below.</p>
			<a href="mailto:media@festival-one.co.nz" class="button">media@festival-one.co.nz</a>
		</div>
		<div class="four-col">-->
		</div>
	</div>

</div>


<?php get_footer(); ?>