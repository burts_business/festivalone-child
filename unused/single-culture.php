<?php get_header(); ?>

<div class="content">
	
	<div class="left">
		<a href="javascript:history.go(-1)"><h2 class="blue"><?php
$category = get_the_category(); 
echo $category[0]->cat_name;
?></h2></a>
		<p>A celebration of being made in the image of the creator God. Bringing our own creativity to join with others. Create. Display. Contribute.</p>
			<a title="Share" target="_blank" class="button" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>">Share <?php the_title(); ?></a>
	</div>
	<div class="right">   	          
                
                  
                  
	<div class="post-container">
		<div class="post-container-title">
			<h1><?php the_title();?></h1>
		</div>
		<div class="post-left">
			<?php 
			$image = get_field('main_image');
			if( !empty($image) ): ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
		</div>
		<div class="post-right">
			<?php the_content()?>
			<!--<a class="button" href="<?php the_field('share_link'); ?>">SHARE</a>-->
		</div>
	</div>
	
            
	</div>
</div>

<?php get_footer(); ?>
