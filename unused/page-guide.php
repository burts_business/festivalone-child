<?php get_header(); ?>

<div class="content clearfix page">
	
	<div class="latest-post">
		<h1>Your Guide to Festival One 2016</h1>
		<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
	
	</div>
	
	<section class="clearfix">
		<div class="intro">
			<h2>Before the Festival</h2> 
			<p>Something here</p>
		</div>
		<div class="main">
			
			<div class="full-width">
				<h3>A little something</h3>
				<p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Sed posuere consectetur est at lobortis.</p>
		
			</div>
			<div class="third">
				<h3>A little something</h3>
				<p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Sed posuere consectetur est at lobortis.</p>
			</div>
			
		
			
			<div class="third">
				<h3>A little something</h3>
				<p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Sed posuere consectetur est at lobortis.</p>
			</div>
			<div class="third">
				<h3>A little something</h3>
				<p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Sed posuere consectetur est at lobortis.</p>
			</div>
			
		</div>
	</section>
	
</div>


<?php get_footer(); ?>