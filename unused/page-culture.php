<?php get_header(); ?>

<div class="left">
		<h2 class="blue"><?php the_title(); ?></h2>
		<p><?php the_field('page_blurb'); ?></p>
		<div class="fb-share-button" data-href="https://festival-one.co.nz" data-width="250px"></div>
	</div>

<div class="content">
	
	
	<div class="right">   	          
               

<?php query_posts( 'cat=3', 'posts_per_page=-1' ); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="post-container">
		<div class="post-container-title">
			<h1><?php the_title();?></h1>
		</div>
		<div class="post-left">
			<?php 
			$image = get_field('main_image');
			if( !empty($image) ): ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
		</div>
		<div class="post-right">
			<?php the_content()?>
			<!--<a class="button" href="<?php the_field('share_link'); ?>">SHARE</a>-->
		</div>
	</div>
	

<?php endwhile; endif; ?>

            
	</div>
</div>

<?php get_footer(); ?>

