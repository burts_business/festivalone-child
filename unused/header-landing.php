<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php if ( is_category() ) {
      echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
    } elseif ( is_tag() ) {
      echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
    } elseif ( is_archive() ) {
      wp_title(''); echo ' Archive | '; bloginfo( 'name' );
    } elseif ( is_search() ) {
      echo 'Search for &quot;'.esc_html($s).'&quot; | '; bloginfo( 'name' );
    } elseif ( is_home() || is_front_page() ) {
      bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
    }  elseif ( is_404() ) {
      echo 'Error 404 Not Found | '; bloginfo( 'name' );
    } elseif ( is_single() ) {
      wp_title('');
    } else {
      echo wp_title( ' | ', 'false', 'right' ); bloginfo( 'name' );
    } ?></title>
    



    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/reset.css" />    
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/landing.css" />
 
	<link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:200,300,400' rel='stylesheet' type='text/css'>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
	
	<script type="text/javascript" src="//use.typekit.net/hgn5cnl.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	
	
	<script type="text/javascript">
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	</script>
	
	<script type="text/javascript">
var fixed = false;

$(document).scroll(function() {
    if( $(this).scrollTop() > 300 ) {
        if( !fixed ) {
            fixed = true;
            $('.secondary').css({ marginTop: '0px'});

        }
    } else {
        if( fixed ) {
            fixed = false;
			$('.secondary').css({ marginTop: '-150px'});
        }
    }
    
   
});
</script>

    <?php wp_head(); ?>
    
  </head>
  
  <body <?php body_class(); ?>>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54068607-1', 'auto');
  ga('send', 'pageview');

</script>
  <div id="landing">
  <div class="off-canvas-wrap">
  	<div class="inner-wrap">  
	  <header>
	  	<div class="top-lock">
	  		<div class="secondary full-width horizontal center">
	  		</div>

	  	</div>
		<div class="top-info full-width horizontal center">
			<ul>
				<a href="/what-is-festival-one"><li>What is Festival One</li></a>
				<a href="/share/"><li>Spread the Word</li></a>
			</ul>
		</div>	  
		<div class="logo center">
			<a href="<?php echo home_url(); ?>"><h1><img src="<?php bloginfo('stylesheet_directory'); ?>/images/fesitval-logo.svg" alt="Festival One" title="Festival One 2015" /></h1></a>
		</div>
		<nav class="full-width horizontal center">
		<ul>
				<li><a href="<?php echo home_url(); ?>/music/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/music.svg" alt="Music" /></a></li>
				<li><a href="<?php echo home_url(); ?>/art/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/art.svg" alt="Art" /></a></li>
				<li class="fixer"><a href="<?php echo home_url(); ?>/community/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/community.svg" alt="Community" /></a></li>
				<a class="hidden-tix" href="<?php echo home_url(); ?>/buy-tickets/"><li style="padding-left:40px;"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/tickets.svg" alt="Buy Tickets" /></li></a>
			</ul>		
		</nav>
		<div class="social-box">
			<ul>
					<a href="https://www.facebook.com/pages/Festival-One/271356039724321?ref=hl" target="_blank"><li>facebook</li></a>
				<a href="https://twitter.com/festivalone_" target="_blank"><li>twitter</li></a>
				<a href="http://instagram.com/festival_one" target="_blank"><li>instagram</li></a>
				<li><i>#festivalone2015</i></li>
			</ul>
		</div>
	</header>

	<section class="container" role="document">