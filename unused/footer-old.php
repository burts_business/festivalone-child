	</div>
</section>
<div class="announcment center blue spacing	">
	<h2 class="blue">Sign up to our mailing list.</h2>
	<p>be the first to find out</p>
	<div class="formwrap"><?php echo do_shortcode('[contact-form-7 id="48" title="Announcments"]'); ?></div>
	<p><i>press enter to Send</i></p>

</div>
<footer class="center">
	<img class="footer-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-clean.svg" alt="Festival One" />
	<div class="top-info full-width horizontal center blue">
			<ul>
				<a href="/info/code-of-conduct/"><li>Code of Conduct</li></a>
				<a href="/terms-and-conditions/"><li>Terms &amp; Conditions</li></a>
				<a href="/press/"><li>Press Package</li></a>
				<a href="/contact/"><li>Contact Us</li></a>
			</ul>
		</div>	  
	<img class="footer-img" src="<?php bloginfo('stylesheet_directory'); ?>/images/hashtag.svg" alt="festivalone2015" />
</footer>

<a class="exit-off-canvas"></a>

  </div>
</div>

<script src="<?php bloginfo('stylesheet_directory'); ?>/js/instafeed.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/main.js"></script>
<?php wp_footer(); ?>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0018/6229.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</body>
</html>