<?php get_header(); ?>

<div class="content">
	
	<div class="left">
		<h2 class="blue">MUSIC</h2>
		<p><?php the_field('page_blurb'); ?></p>
	</div>
	<div class="right">   	          
                
                <?php  $args = array( 'post_type' => 'artists', 'posts_per_page' => 999, 'order' => 'ASC' );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();?>
	
	<div class="post-container">
		<div class="post-container-title">
			<a href="<?php the_permalink(); ?>"><h1><?php the_title();?></h1></a>
		</div>
		<div class="post-left">
			<!--<?php 
			$image = get_field('main_image');
			if( !empty($image) ): ?>
				<a href="<?php the_permalink();?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
			<?php endif; ?>-->
		</div>
		<div class="post-right">
			<!--<?php the_excerpt(); ?>
			<!--<a class="button" href="<?php the_permalink(); ?>">Read More</a>-->
		</div>
	</div>
	

<?php endwhile; ?>
            
	</div>
</div>

<?php get_footer(); ?>
