<?php get_header(); ?>

</div>
				

	<div class="topdiv">	
			<div class="my-slider">
				<ul>
					<?php
						// check if the repeater field has rows of data
						if( have_rows('slider') ): ?>
						 <?php	// loop through the rows of data
						    while ( have_rows('slider') ) : the_row(); ?>
					<li>
					
						<div class="slide">
							
							<div class="image-header">
								<img src="<?php the_sub_field('slider_image'); ?>" alt="" />
								<div class="home-title">
									<h2><?php the_sub_field('slider_heading');?></h2>
									<p><?php the_sub_field('slider_copy');?></p>
								</div>
							</div>
						</div>
						
					</li>
					<?php endwhile; ?>
					<?php endif;	
					?>
				</ul>
			</div>
		</div>
	

<div class="content">
	<div class="tickets clearfix content">
		<div class="ticket-box">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016" target="_blank">
				<div class="price">
					<h3>Child</h3>
					<p>$75</p>
				</div>
			</a>
			<p>5 - 12 Years Old</p>
		</div>
		<div class="ticket-box">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016" target="_blank">
				<div class="price">
					<h3>Student</h3>
					<p>$119</p>
				</div>
			</a>
			<p>With valid Student I.D.</p>
		</div>
		<div class="ticket-box">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016" target="_blank">
				<div class="price">
					<h3>Adult</h3>
					<p>$144</p>
				</div>
			</a>
			<i></i>
		</div>
		<div class="ticket-box">
			<a href="https://www.iticket.co.nz/events/2016/feb/festival-one-2016" target="_blank">
				<div class="price">
					<h3>Family</h3>
					<p>Price Varies</p>
				</div>
			</a>
			<p>Click for details</p>
		</div>

		<div class="ticket-box">
			<a href="https://www.iticket.co.nz/events/2015/apr/festival-one-2016-campervan-and-caravan-sites" target="_blank">
				<div class="price">
					<h3>Caravans</h3>
					<p>On sale now</p>
				</div>
			</a>
			<p>Click for details</p>
		</div>
	</div>
	
	<div class="latest-post">
		<p class="intro">Latest Post</p>
		<?php $args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'order' => 'DESC' );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); ?>
				
				 <h2><?php the_title(); ?></h2>
				 <p><?php echo substr(get_the_excerpt(), 0,250); ?>...</p>
				
				<?php endwhile; ?>
				
		<a href="<?php echo home_url(); ?>/news/">See all News</a>
	</div>
	
	<div class="trailer-container content">
		<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='http://player.vimeo.com/video/126242942?title=0&amp;byline=0&amp;portrait=0' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
		
	</div>
	
	<div class="more-info">
		<p>For more information check out our <span><a href="<?php echo home_url(); ?>/info">info page</a></span> <br>
or see the current list of artists and speakers under <span><a href="<?php echo home_url(); ?>/lineup">lineup</a></span></p>
	</div>


	<a href="http://instagram.com/festival_one" target="_blank">
	
		<img class="hashtag-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/hashtag.svg" alt="Buy Tickets Now"/>
	</a>
	
	<div id="instafeed" class="content"></div>



<?php get_footer(); ?>