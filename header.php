<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php if ( is_category() ) {
      echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
    } elseif ( is_tag() ) {
      echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
    } elseif ( is_archive() ) {
      wp_title(''); echo ' Archive | '; bloginfo( 'name' );
    } elseif ( is_search() ) {
      echo 'Search for &quot;'.esc_html($s).'&quot; | '; bloginfo( 'name' );
    } elseif ( is_home() || is_front_page() ) {
      bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
    }  elseif ( is_404() ) {
      echo 'Error 404 Not Found | '; bloginfo( 'name' );
    } elseif ( is_single() ) {
      wp_title('');
    } else {
      echo wp_title( ' | ', 'false', 'right' ); bloginfo( 'name' );
    } ?></title>
    <meta property="og:title" content="Festival One 2016" />
    <meta property="og:site_name" content="Festival One" />
	<meta property="og:url" content="http://festival-one.co.nz" />
	<meta property="og:image" content="http://festival-one.co.nz/festival-one.png" />
	<meta property="og:description" content="Festival One. Mystery Creek, Hamilton. January 29th - Feb 1st 2016. Music, art and community. A place for the church in New Zealand to have fun, learn, and grow." />
	<meta property="article:publisher" content="https://www.facebook.com/pages/Festival-One/271356039724321?ref=hl" />
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/reset.css" />  
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/twenty-fifteen.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/twenty-sixteen.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/twenty-seventeen.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/unslider.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/unslider-dots.css" />


    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
	<link href='http://fonts.googleapis.com/css?family=Source+Code+Pro:200,300,400' rel='stylesheet' type='text/css'>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
	
	<script type="text/javascript" src="//use.typekit.net/hgn5cnl.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/unslider.js"></script>
	<script>
		jQuery(document).ready(function($) {
			$('.my-slider').unslider({
				autoplay: true,
				infinite: true,
				arrows: false
			});
			
		});
	</script>
		
    <?php wp_head(); ?>
    
  </head>
  
  <body <?php body_class(); ?>>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54068607-1', 'auto');
  ga('send', 'pageview');

</script>
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=206472836086996&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="off-canvas-wrap">
	<div class="inner-wrap">  
 
 


 

	  <div class="border">
		  
		<div class="mobile-nav">
			<ul>
			  	<li><a href="<?php echo home_url(); ?>/lineup/" style="color:white;">Lineup</a></li>
			  	<li><a href="<?php echo home_url(); ?>/community/" style="color:white;">Community</a></li>
			  	<li><a href="<?php echo home_url(); ?>/guide/" style="color:white;">Guide</a></li>
			  	<li><a href="<?php echo home_url(); ?>/info/" style="color:white;">Info</a></li>
			  	<li><a href="<?php echo home_url(); ?>/news/" style="color:white;">News</a></li>
			  	<li><a href="<?php echo home_url(); ?>/buy-tickets/" style="color:white;">Buy Tickets</a></li>
			</ul>
			<img id="close" class="close" src="<?php bloginfo('stylesheet_directory'); ?>/images/close.svg" alt="close" title="Close" />
		</div>
	  	<div class="content">
		  	
	  	<?php if(is_front_page() ) { ?>
		<header>
		  <nav> 

			<ul>
			  	
			  	<li><a href="<?php echo home_url(); ?>/lineup/" style="color:white;" >Lineup</a></li>
			  	<li><a href="<?php echo home_url(); ?>/community/" style="color:white;" >Community</a></li>
			  	<li><a href="<?php echo home_url(); ?>/guide/" style="color:white;" >Guide</a></li>
			  		
			  	<li><a href="<?php echo home_url(); ?>/info/" style="color:white;" >Info</a></li>
			  	
			  	<li><a href="<?php echo home_url(); ?>/news/" style="color:white;" >News</a></li>
			  	<li><a href="<?php echo home_url(); ?>/buy-tickets/" style="color:white;" >Buy Tickets</a></li>
		  	</ul>

			
	  	</nav>
	  	
		<div class="logo white center">
			<h1>
				<a href="<?php echo home_url(); ?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-white.png" alt="Festival One" title="Festival One 2016" />
				</a>
			</h1>
		</div>
	
		<div class="social-box">
			<ul>
				<a class="trigger"><li><img src="<?php bloginfo('stylesheet_directory'); ?>/images/menu-white.svg" alt="Menu" /></li></a>
				<a class="social" href="https://www.facebook.com/pages/Festival-One/271356039724321?ref=hl" target="_blank"><li style="color: white;">facebook</li></a>
				<a class="social" href="https://twitter.com/festivalone_" target="_blank"><li style="color: white;">twitter</li></a>
				<a class="social" href="http://instagram.com/festival_one" target="_blank"><li style="color: white;">instagram</li></a>
			</ul>
		</div>
		
	</header>
	
	

<?php } else { ?>


<header>
		  <nav> 

		  	<ul>
			  	
			  	<li><a href="<?php echo home_url(); ?>/lineup/">Lineup</a></li>
			  	<li><a href="<?php echo home_url(); ?>/community/">Community</a></li>
			  	<li><a href="<?php echo home_url(); ?>/guide/">Guide</a></li>
			  		
			  	<li><a href="<?php echo home_url(); ?>/info/">Info</a></li>
			  	
			  	<li><a href="<?php echo home_url(); ?>/news/">News</a></li>
			  	<li><a href="<?php echo home_url(); ?>/buy-tickets/">Buy Tickets</a></li>
		  	</ul>
	  	</nav>
		<div class="logo center">
			<h1>
				<a href="<?php echo home_url(); ?>">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Festival One" title="Festival One 2016" />
				</a>
			</h1>
		</div>
	
		<div class="social-box">
			<ul>
				<a class="trigger"><li><img src="<?php bloginfo('stylesheet_directory'); ?>/images/menu.svg" alt="Menu" /></li></a>
				<a class="social" href="https://www.facebook.com/pages/Festival-One/271356039724321?ref=hl" target="_blank"><li>facebook</li></a>
				<a class="social" href="https://twitter.com/festivalone_" target="_blank"><li>twitter</li></a>
				<a class="social" href="http://instagram.com/festival_one" target="_blank"><li>instagram</li></a>
			</ul>
		</div>
		
	</header>
	
<?php } ?>


	