<?php get_header(); ?>

<div class="content">	
	<div class="image-header">
		<?php if ( has_post_thumbnail() ) { 
				the_post_thumbnail('header'); 
		}?>
		<h1><?php the_title();?></h1>
	</div>


	<article class="lineup single clearfix">
		
		<div class="left">
			<a href="/news/"><h4><?php the_title();?></h4></a>
			<p>Keep up to date with the latest announcements and hottest news for Festival One 2016</p>
				<a title="Share" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>">Share post</a>
		</div>
		
		<div class="right">   	             
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>    
				
				<div class="post-container">
					<div class="post-container-title">
		
					</div>
					<?php the_content()?>
				</div>
				
				<?php endwhile; ?>
			<?php endif; ?>
		
		</div>
		<div class="clear clearfix"></div>
	</article>
</div>

<?php get_footer(); ?>
