<?php get_header(); ?>

<div id="ticket-page" class="content clearfix page">
	
	<div class="latest-post">
		<h1>Festival One Shop</h1>
		<p>You heard right, Festival One now has a little online store. Grab yourself some sweet merch below.</p>
		<a title="Share" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>">Share this page</a>
	</div>
	
	<!--<section class="clearfix">
		
		<div class="third">
			<div data-embed_type="product" data-shop="festival-one.myshopify.com" data-product_name="Festival One Icon Tee" data-product_handle="festival-one-icon-tee" data-has_image="true" data-display_size="compact" data-redirect_to="checkout" data-buy_button_text="Buy now" data-buy_button_out_of_stock_text="Out of Stock" data-buy_button_product_unavailable_text="Unavailable" data-button_background_color="0f346b" data-button_text_color="ffffff" data-product_modal="false" data-product_title_color="000000" data-next_page_button_text="Next page"></div>
			<script type="text/javascript">
			document.getElementById('ShopifyEmbedScript') || document.write('<script type="text/javascript" src="https://widgets.shopifyapps.com/assets/widgets/embed/client.js" id="ShopifyEmbedScript"><\/script>');
			</script>
			<noscript><a href="https://festival-one.myshopify.com/cart/16354544513:1" target="_blank">Buy Festival One Icon Tee</a></noscript>
		</div>
		<div class="third">
			<div data-embed_type="product" data-shop="festival-one.myshopify.com" data-product_name="Festival One 2016 Band Tee" data-product_handle="festival-one-2016-band-tee" data-has_image="true" data-display_size="compact" data-redirect_to="checkout" data-buy_button_text="Buy now" data-buy_button_out_of_stock_text="Out of Stock" data-buy_button_product_unavailable_text="Unavailable" data-button_background_color="0f346b" data-button_text_color="ffffff" data-product_modal="false" data-product_title_color="000000" data-next_page_button_text="Next page"></div>
			<script type="text/javascript">
			document.getElementById('ShopifyEmbedScript') || document.write('<script type="text/javascript" src="https://widgets.shopifyapps.com/assets/widgets/embed/client.js" id="ShopifyEmbedScript"><\/script>');
			</script>
			<noscript><a href="https://festival-one.myshopify.com/cart/16355351489:1" target="_blank">Buy Festival One 2016 Band Tee</a></noscript>
		</div>
		<div class="third">
			<div data-embed_type="product" data-shop="festival-one.myshopify.com" data-product_name="Festival One Numbers Tee" data-product_handle="festival-one-numbers-tee" data-has_image="true" data-display_size="compact" data-redirect_to="checkout" data-buy_button_text="Buy now" data-buy_button_out_of_stock_text="Out of Stock" data-buy_button_product_unavailable_text="Unavailable" data-button_background_color="0f346b" data-button_text_color="ffffff" data-product_modal="false" data-product_title_color="000000" data-next_page_button_text="Next page"></div>
			<script type="text/javascript">
			document.getElementById('ShopifyEmbedScript') || document.write('<script type="text/javascript" src="https://widgets.shopifyapps.com/assets/widgets/embed/client.js" id="ShopifyEmbedScript"><\/script>');
			</script>
			<noscript><a href="https://festival-one.myshopify.com/cart/16355409537:1" target="_blank">Buy Festival One Numbers Tee</a></noscript>
		</div>
	
	</section>-->
	<section class="clearfix">
		
		<div data-accent_color="767676" data-background_color="ffffff" data-button_background_color="0f346b" data-button_text_color="ffffff" data-cart_button_text="Cart" data-cart_title="Your cart" data-cart_total_text="Total" data-checkout_button_text="Checkout" data-discount_notice_text="Shipping and discount codes are added at checkout." data-embed_type="cart" data-empty_cart_text="Your cart is empty." data-shop="festival-one.myshopify.com" data-sticky="true" data-text_color="000000"></div>
<div data-background_color="ffffff" data-button_background_color="0f346b" data-button_text_color="ffffff" data-buy_button_out_of_stock_text="Out of Stock" data-buy_button_product_unavailable_text="Unavailable" data-buy_button_text="Buy now" data-collection_handle="t-shirts" data-display_size="compact" data-embed_type="collection" data-has_image="true" data-next_page_button_text="Next page" data-product_handle="" data-product_modal="true" data-product_name="" data-product_title_color="000000" data-redirect_to="modal" data-shop="festival-one.myshopify.com"></div>
<script type="text/javascript">document.getElementById('ShopifyEmbedScript') || document.write('<script type="text/javascript" src="https://widgets.shopifyapps.com/assets/widgets/embed/client.js" id="ShopifyEmbedScript"><\/script>');</script>
	</section>
	

</div>


<?php get_footer(); ?>