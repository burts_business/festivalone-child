<?php get_header(); ?>

<div class="page content">
	<div class="latest-post">
		<p class="intro">Latest Post</p>
		<?php $args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'order' => 'DESC' );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); ?>
				
				 <h2><?php the_title(); ?></h2>
				 <p><?php echo substr(get_the_excerpt(), 0,250); ?>...</p>
				
				
				
		<a href="<?php the_permalink(); ?>">Read more</a>
			<?php endwhile; ?>
	</div>


	<div class="news clearfix">       
	    <?php   $args = array( 'posts_per_page' => 999, 'order' => 'DESC' );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();?>
	       
	            <div class="third">
		            <a href="<?php the_permalink();?>">
						<?php if ( has_post_thumbnail() ) { 
							the_post_thumbnail('header'); 
						}?>
						<h3><?php the_title();?></h3>
					</a>
	            </div>      
			
	
		 <?php endwhile; ?>
	</div>
</div>
<?php get_footer(); ?>
