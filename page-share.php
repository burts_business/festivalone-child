<?php get_header(); ?>

<div class="content">
	<div class="four-col col-left">
		<h2 class="blue">Sharing is Caring</h2>
		<p>We want you to own this. So we aren't doing any advertising. That we are leaving to you. Here is a quick overview of the festival, now you do the rest. Print a poster. Take a gram. Do your thing.</p>
		
	</div>
	<div class="clearfix"></div>
		<div class="col-container">
		<div class="four-col col-left">
			<h2 class="blue">Posters</h2> 
			<p>Print out a poster or two and pin them up at church, paste them on a lamp post (if it's legal?) or hang them in your room. However you do it, make sure you share it around!</p>
			<p><b>#FestivalOne2015</b></p>
		</div>
		<div class="four-col">
			<h3>White</h3>
			<p>A crisp, clean, sharp poster that will stand out among the rest.</p>
			<a href="<?php bloginfo('stylesheet_directory'); ?>/assets/Festvial One Poster-White.pdf" class="button" target="blank">Download</a>	
		</div>
		<div class="four-col">
			<h3>Blue</h3>
			<p>The contrast poster. Because everyone wants to be a little different.</p>
			<a href="<?php bloginfo('stylesheet_directory'); ?>/assets/Festvial One Poster-Blue.pdf" class="button" target="_blank">Download</a>
		</div>
	</div>
	<div class="col-container">
		<div class="four-col col-left blue">
			<h2 class="blue">Festival One - Artists</h2> 
		</div>
		<div class="four-col">
			<h3>Headliners</h3>
			<p>Festival One 2015 Headliners. Switchfoot. Halfnoise. Gungor. With many more to come.</p>
			<a href="http://hrefshare.com/9248d" class="button">Share</a>	
		</div>
		<div class="four-col">
			<h3>Switchfoot</h3>
			<p>Switchfoot return to New Zealand to play at the brand new Festival One.</p>
			<a href="http://hrefshare.com/829eb" class="button">Share</a>
		</div>
		<div class="four-col printed">
			<h3>Halfnoise</h3>
			<p>Highly acclaimed Halfnoise headlining new festival with new music. Festival One.</p>
			<a href="http://hrefshare.com/831c" class="button">Share</a>
		</div>
	</div>
	<div class="col-container">
		<div class="four-col col-left blue">
			<h2 class="blue">Festival One - Speakers</h2> 
		</div>
		<div class="four-col">
			<h3>Rikk E. Watts</h3>
			<p>Dr. Rikk Watts is a world expert on the relationship of the Old and New Testaments. He specialises in the life of Jesus and the Gospels of Mark and John. He is Professor of New Testament at Regent College, Vancouver.</p>
			<a href="http://hrefshare.com/6e94" class="button">Share</a>	
		</div>
	</div>
	<div class="col-container">
		<div class="four-col col-left blue">
			<h2 class="blue">Festival One - Features</h2>
		</div>
		<div class="four-col">
			<h3>Short Film Competition</h3>
			<p>Shoot a clip. Submit it to the judges. And watch the winners on the big screen.</p>
			<a href="http://hrefshare.com/b5869" class="button">Share</a>
		</div>
		<div class="four-col printed">
			<h3>Curate your own Camp Site</h3>
			<p>How can you make your camping site completely unique? Come, build and create your community.</p>
			<a href="http://hrefshare.com/a1f1" class="button">Share</a>
		</div>
		<div class="four-col printed">
			<h3>Free Feed</h3>
			<p>2 Tonnes of beef that has been donated by the local farmers that we will cook, over fire, for you, for free!</p>
			<a href="http://hrefshare.com/5dd7" class="button">Share</a>
		</div>
		
	</div>
</div>


<?php get_footer(); ?>